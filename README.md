# My First Full Stack Web Application
This project required me to utilize all of the information that I have been learning over the past few months and put it all together. It may not be the most exciting app ever created but it is definitely the most exciting app that I have ever personally created. So cheers to new beginnings and a brighter future!


*This project was guided by a tutorial from Dan Bunker in conjunction with Pluralsight. You can find the full tutorial here: https://app.pluralsight.com/library/courses/building-first-app-with-spring-boot-angularjs/table-of-contents*

---

## This project utilizes:

**Languages**

1. Java
2. HTML
3. CSS
4. JavaScript
5. MySQL

**Frameworks**

1. Spring Boot
2. Angular