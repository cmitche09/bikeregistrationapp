package com.globalmatics.bike.repositories;

import com.globalmatics.bike.model.Bike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface BikeRepository extends JpaRepository<Bike, Long> {
}
